module NN
implicit none
private

public :: dispWeights, setWeights, mySigmoidalNetwork

real*8 :: IW(1:4,1:10), LW(1:4, 1:4)
real*8 :: b1(1:4,1), b2(1:4,1)
logical :: weights_set = .false.
real*8 :: pi

contains

function dispWeights()
	integer dispWeights, i, j

	if(weights_set) then
		dispWeights = 1
		write(*,*) 'IW Matrix:'
		do j = 1, 4
			do i = 1, 10
				write(*,'(E22.12E3, A)', ADVANCE='NO') IW(j, i)
			enddo
			write(*, '(A)', ADVANCE='NO') CHAR(10)
		enddo
		write(*,*) 'LW Matrix:'
		do j = 1, 4
			do i = 1, 4
				write(*,'(E22.12E3, A)', ADVANCE='NO') LW(j, i)
			enddo
			write(*, '(A)', ADVANCE='NO') CHAR(10)
		enddo
		write(*,*) 'b1 Matrix:'
		do j = 1, 4
			write(*,'(E22.12E3, A)', ADVANCE='NO') b1(j, 1)
			write(*, '(A)', ADVANCE='NO') CHAR(10)
		enddo
		write(*,*) 'b1 Matrix:'
		do j = 1, 4
			write(*,'(E22.12E3, A)', ADVANCE='NO') b2(j, 1)
			write(*, '(A)', ADVANCE='NO') CHAR(10)
		enddo


	else
		write(*,*) 'Weights not set!'
		dispWeights = 0
	endif
end function dispWeights

function setWeights(IW_set, b1_set, LW_set, b2_set)
	integer setWeights
	real*8 IW_set(1:4,1:10), b1_set(1:4,1), b2_set(1:4,1), LW_set(1:4, 1:4)
	IW = IW_set
	b1 = b1_set
	LW = LW_set
	b2 = b2_set
	pi = 4*atan(1.0d0)
	write(*,*) 'weights set ', IW, b1, LW, b2
	weights_set = .true.
	setWeights = 0
end function

function mySigmoidalNetwork(input)
	real*8, dimension(4,1) :: mySigmoidalNetwork
	real*8, dimension(10,1) :: input
	real*8, dimension(4,1) :: input_result, h
	real*8, dimension(10,1) :: in;

	in = input;

	!Scale input
	!in = (min(max(in, -5), 5));
	in = in/5;
	input_result = matmul(IW,in)+B1;
	!input_result = (min(max(input_result, -5), 5));
	input_result = tanh(input_result);
	input_result = matmul(LW,input_result) + b2;
	mySigmoidalNetwork = input_result*pi;

	write(*,*) 'Neural Net Eval', mySigmoidalNetwork
end function mySigmoidalNetwork
end module


module BipedStuff
use NN
implicit none
real*8 NaN, pi
real*8 g, mb, mt, ms, Ib, It, Is, L_t, L_s, Lb, Cb, Ct, Cs, K_g, K_gh, B_g, Tpoint1, Tpoint2

contains

function Biped(X, simlength, controller, X_rec, t_rec)
real*8 X(1:14)
real*8 simlength
integer controller
integer Biped
real*8 X_rec(:, :), t_rec(:)


real*8 pi, NaN
real*8 Tcontrol(1:4), p1(1:2), p2(1:2), out(1:4), X_bar(1:14), Xd(1:14), Xd_1(1:14)
real*8 input_sample(1:10, 1), output_sample(1:4,1)
real*8 p_lead(1:2), p_lag(1:2), Tt_lead, Tt_lag, Ts_lead, Ts_lag
real*8 RTb, RT1, RT2, RS1, RS2, T1, T2, xh, yh, Tb, Tt1, Tt2, Ts1, Ts2
real*8 xhd, yhd, Tbd, Tt1d, Tt2d, Ts1d, Ts2d, dt1, dt, tf, totAng, totX
real*8 gzone, C1, C2, RT_lead, RT_lag, RS_lead, RS_lag
integer i, j, sample, nt, N, retval
real*8 IW(1:4,1:10), b1(1:4,1), b2(1:4,1), LW(1:4, 1:4)
real*8 Kbod, Kleg, Kshank, Bleg

g = 9.8d0
mb = 8d0
mt = 1d0
ms = 1d0
Ib = .1d0
It = .01d0
Is = .01d0
Lb = .6d0
L_t = .4d0
L_s = .4d0
Cb = .3d0
Ct = .2d0
Cs = .4d0
K_g = 8000d0
K_gh = 1000d0
B_g = 10000d0

NaN = -1.0
NaN = sqrt(NaN)
Tpoint1 = NaN
Tpoint2 = NaN
pi = 4*atan(1.0d0)

IW = transpose(reshape((/-0.5602, -0.0497, 0.0837, 0.2549, -0.1822, -0.1865, 0.3087, 0.1787, 8.5096, 5.6566, &
	0.0412, 0.0019, -0.0042, -0.0019, 0.0077, 0.0121, -0.0164, -0.0143, 0.5456, 0.3565, &
	-33.4934, -0.0576, 3.9968, 13.9683, -34.2255, -35.1777, -7.8284, -9.1547, -26.1502, 39.6703, &
	 0.0916, 0.0072, -0.0133, 0.1156, 0.0188, 0.0909, -0.0856, -0.1212, -13.7341, 2.8549 /), shape(transpose(IW))))

LW = transpose(reshape((/-0.0671, -0.5521, -0.0006, 0.1368, &
 			0.0747, 0.9822, 0.0002, -0.1274, &
    		0.3076, -1.3300, -0.4027, 0.1895, &
    		0.0412, 1.3783, -0.0015, 0.2908/), shape(transpose(LW))))
b1 = reshape((/-0.1871, -0.3889, 0.5279, 1.5535/), shape(b1))
b2 = reshape((/0.0325, 0.3682, -0.8755, 0.1022/), shape(b1))


!Open (unit=0, file='psi0.csv', status='unknown')

RTb = 0d0;
RT1 = 0d0;
RT2 = 0d0;
RS1 = 0d0;
RS2 = 0d0;
T1 = 0d0;
T2 = 0d0;


!initial conditions
tf = 0d0
!X = (/ -.4d0,.8d0,0d0,1.3d0,-.1d0,0d0,0d0,.5d0,0d0,0d0,0d0,0d0,0d0,0d0 /)


!Simulation Loop
do i = 1, 10000, 1

	!write(*,*) 'X:', X
    !Position States
    xh = X(1);
    yh = X(2);
    Tb = X(3);
    Tt1 = X(4);
    Tt2 = X(5);
    Ts1 = X(6);
    Ts2 = X(7);

    !Velocity States
    xhd = X(8);
    yhd = X(9);
    Tbd = X(10);
    Tt1d = X(11);
    Tt2d = X(12);
    Ts1d = X(13);
    Ts2d = X(14);

	t_rec(i) = tf
	X_rec(i, :) = X

	!end conditions
	if (tf > simlength) then
		write(*,*) 'Simulation Timeout'
		exit
	endif
	if (yh < 0) then
		write(*,*) 'Biped Fell'
		exit
	endif
	if (abs(Tb) > pi/2) then
		write(*,*) 'Body Tilt'
		exit
	endif
	if (isNan(X(1))) then
		write(*,*) 'Numerical Error Termination'
		exit
	endif


   !dt_sum = dt_sum + dt1;
   dt1 = .01d0;

   !Simulate
   !TOUT = 0;
   !YOUT = 0;

   !Tcontrol = [0 0 0 0];

   !simple walking controller

   p1 = (/xh+L_t*sin(Tt1)+L_s*sin(Ts1), yh-L_t*cos(Tt1)-L_s*cos(Ts1)/)
   p2 = (/xh+L_t*sin(Tt2)+L_s*sin(Ts2), yh-L_t*cos(Tt2)-L_s*cos(Ts2)/)

   gzone = .05;
   !Touchdown point tracking
   if(p1(2) < gzone .AND. isnan(Tpoint1)) then
       Tpoint1 = p1(1);
   elseif (p1(2) > gzone .AND. .NOT.isnan(Tpoint1)) then
        	Tpoint1 = NaN;
   endif

   if(p2(2) < gzone .AND. isnan(Tpoint2)) then
       Tpoint2 = p2(1);
   elseif (p2(2) > gzone .AND. .NOT.isnan(Tpoint2)) then
       		Tpoint2 = NaN;
   endif
   !disp( ['Tpoint1 = ', num2str(Tpoint1), 'Tpoint2 = ', num2str(Tpoint2)]);
   !pause
   !disp(horzcat('Sim Time = ', num2str(tf), '/', num2str(simlength)))

   if(Tt1 > Tt2) then
       p_lead = p1;
       p_lag = p2;
       Tt_lead = Tt1;
       Tt_lag = Tt2;
       Ts_lead = Ts1;
       Ts_lag = Ts2;
   else
       p_lead = p2;
       p_lag = p1;
       Tt_lead = Tt2;
       Tt_lag = Tt1;
       Ts_lead = Ts2;
       Ts_lag = Ts1;
   endif

   !Determine contact state
   if(p_lead(2) < gzone) then
       C1 = 1;
   else
       C1 = 0;
   endif
   if(p_lag(2) < gzone) then
       C2 = 1;
   else
       C2 = 0;
   endif

   input_sample = reshape((/yh, xhd, yhd, Tb, Tt1, Tt2, Ts1, Ts2, C1, C2 /), shape(input_sample))

   if(controller == 1) then
       if(p_lead(2) < gzone) then
           if(p_lag(2) < gzone) then !double support
               RTb = -.2d0;
               RT_lead = 0d0;
               RT_lag = 1d0;
               RS_lead = 0d0;
               RS_lag = -.9d0;
           else !Single support on leading leg
               RTb = -.1d0;
               RT_lead = 0d0;
               RT_lag = .9d0;
               RS_lead = 0d0;
               if(Tt_lag<.6) then
               		RS_lag = -1.5d0;
               else
               		RS_lag = 1d0;
               endif
           endif
       else
           if(p_lag(2) < gzone) then !Single support on trailing leg
               RTb = -.1d0;
               RT_lead = .9d0;
               RT_lag = 0d0;
               if(Tt_lead<.6) then
               		RS_lead = -1.5d0;
               else
               		RS_lead = 1d0;
               endif
               RS_lag = 0;
           else !Flying
               RTb = -.1d0;
               RT_lead = 1.2d0;
               RT_lag = -.4d0;
               RS_lead = .2d0;
               RS_lag = -.4d0;
           endif
       endif
   elseif (controller == 2) then
        !out1 = sim(testnet, input_sample');
		!retval = dispWeights()
        out = reshape(mySigmoidalNetwork(input_sample), shape(out));
        !out1-out
        !out = out1;

        RT_lead = out(1);
        RT_lag = out(2);
        RS_lead = out(3);
        RS_lag = out(4);
        !RTb = out(5);
   else
       RT_lead = 0;
       RT_lag = 0;
       RS_lead = 0;
       RS_lag = 0;
   endif

   output_sample = reshape((/RT_lead, RT_lag, RS_lead, RS_lag/), shape(output_sample))

   if(Tt1 > Tt2) then
       RT1 = RT_lead;
       RT2 = RT_lag;
       RS1 = RS_lead;
       RS2 = RS_lag;
   else
       RT2 = RT_lead;
       RT1 = RT_lag;
       RS2 = RS_lead;
       RS1 = RS_lag;
   endif



   Kbod = 1000;
   Kleg = 600;
   Kleg = 400;
   Kshank = 300;
   Kshank = 400;
   Bleg = 30;

   Tcontrol = (/Kleg*(RT1-Tt1)-Kbod*(RTb-Tb)-Bleg*(Tt1d), &
   				Kleg*(RT2-Tt2)-Kbod*(RTb-Tb)-Bleg*(Tt2d), &
   				Kshank*(RS1-Ts1)-Bleg*(Ts1d-Tt1d), &
   				Kshank*(RS2-Ts2)-Bleg*(Ts2d-Tt2d)/)
   !write(*,*) 'Control Torque', Tcontrol
   !write(*,*) 'X=', X
   !write(*,*) 'Tcontrol=', Tcontrol
   !write(*,*) 'Tpoints', Tpoint1, Tpoint2

   !NNinput(sample, :) = X';
   !NNoutput(sample, :) = F;
   sample = sample + 1;

   !some ode method
   !options = odeset('RelTol',1e-3,'AbsTol',1e-3*ones(1,14));
   ![TOUT, YOUT] = ode45(@FiveLinkEOM, [0 dt1], X, options);
   !X = YOUT(size(YOUT, 1), :);

   !Alternative methods
   nt = 10;
   dt = dt1/(nt);

   do j = 1,nt
       !NNinput = [NNinput ; FiveLinkEOM(0, X)'];
       !NNoutput = [NNoutput ; F];
       !forward euler
       !X = X + FiveLinkEOM(0, X)*dt;

       !midpoint method
       !X = X+dt*FiveLinkEOM(0, X+0.5*dt*FiveLinkEOM(0, X));

       !Heuns Method
       Xd = FiveLinkEOM(0d0, X, Tcontrol)
       !write (*,*) 'Xd: ', Xd
       X_bar = X + dt*Xd;
       !write (*,*) 'Xbar: ', X_bar
       Xd_1 = FiveLinkEOM(0d0, X_bar, Tcontrol)
       !write (*,*) 'Xd_1: ', Xd_1
       X = X + .5*dt*(Xd+Xd_1);
       !write (*,*) 'X: ', X
   enddo

   tf = tf+dt1;
   !tf = toc(st);
!time(i) = tf;
!Xt(:,i) = X;


enddo

!N = i+1
!do i=1, N
!	write(*,*) 'X(', t_rec(i), ') = ',  X_rec(i, :)
!enddo


!do j = 1, N, 1
!	write(1,'(E22.12E3, A)', ADVANCE='NO') t_rec(j), ','
!	do i = 1, 14, 1
!		write(1,'(E22.12E3, A)', ADVANCE='NO') X_rec(j, i), ','
!	enddo
!	write(1,'(A)', ADVANCE='NO') CHAR(10)
!enddo

Biped = i
end function Biped


!Functions Section

!logical function isnan(a)
!real a
!if (a.ne.a) then
!	isnan = .true.
!else
!	isnan = .false.
!endif
!
!return
!end function isnan


function  ones(rows, cols)
	integer rows, cols
	real*8, dimension(rows,cols) :: ones
	ones = 1d0
end function ones



function FiveLinkEOM(t, X, Tcontrol)
	real*8 FiveLinkEOM(1:14), X(1:14), Tcontrol(1:4), Tcontrol_clipped(1:4)
	real*8 t, xh, yh, Tb, Tt1, Tt2, Ts1, Ts2, xhd, yhd, Tbd, Tt1d, Tt2d, Ts1d, Ts2d
	real*8 cos_Tb, sin_Tb, cos_Tt1, sin_Tt1, cos_Tt2, sin_Tt2, cos_Ts1, sin_Ts1, cos_Ts2, sin_Ts2
	real*8 mt1, mt2, ms1, ms2, Ct1, Ct2, Cs1, Cs2, Is1, Is2, GS1, GS2
	real*8 Tmax, N1, N2
	real*8, dimension(14,1) :: Xmat
	real*8, dimension(7,7) :: M, C
	real*8, dimension(7,1) :: Gvec, Tg, N, temp, Xdd
	real*8, dimension(2) :: p1, p2, v1, v2, F1, F2

	integer result, IPIV(1:7)


	Xmat = reshape(X, shape(Xmat))
	!XD = 0*X;
	!return

	Tmax = 50d0;
	!Saturation
	!Tcontrol = max(min(Tcontrol, ones(1,4)*Tmax), -ones(1,4)*Tmax);
	Tcontrol_clipped = max(min(Tcontrol, (/Tmax, Tmax, Tmax, Tmax/)), -(/Tmax, Tmax, Tmax, Tmax/));

	!Position States
	xh = X(1);
	yh = X(2);
	Tb = X(3);
	Tt1 = X(4);
	Tt2 = X(5);
	Ts1 = X(6);
	Ts2 = X(7);

	!Velocity States
	xhd = X(8);
	yhd = X(9);
	Tbd = X(10);
	Tt1d = X(11);
	Tt2d = X(12);
	Ts1d = X(13);
	Ts2d = X(14);

	cos_Tb = cos(Tb);
	sin_Tb = sin(Tb);
	cos_Tt1 = cos(Tt1);
	sin_Tt1 = sin(Tt1);
	cos_Tt2 = cos(Tt2);
	sin_Tt2 = sin(Tt2);
	cos_Ts1 = cos(Ts1);
	sin_Ts1 = sin(Ts1);
	cos_Ts2 = cos(Ts2);
	sin_Ts2 = sin(Ts2);




	mt1 = mt;
	mt2 = mt;
	ms1 = ms;
	ms2 = ms;
	Ct1 = Ct;
	Ct2 = Ct;
	Cs1 = Cs;
	Cs2 = Cs;
	Is1 = Is;
	Is2 = Is;

	!write(*,*) mb, mt1, mt2, ms1, ms2


	M = transpose(reshape((/mb+mt1+mt2+ms1+ms2, 0d0, -mb*Cb*cos_Tb, mt1*Ct*cos_Tt1+ms1*L_t*cos_Tt1, mt2*Ct*cos_Tt2+ms2*L_t*cos_Tt2,&
			 ms1*Cs*cos_Ts1, ms2*Cs*cos_Ts2, &
	     0d0, mb+mt1+mt2+ms1+ms2, -mb*Cb*sin_Tb, mt1*Ct*sin_Tt1+ms1*L_t*sin_Tt1, mt2*Ct*sin_Tt2+ms2*L_t*sin_Tt2, &
	     	 ms1*Cs*sin_Ts1, ms2*Cs*sin_Ts2, &
	     -Cb*mb*cos_Tb, -Cb*mb*sin_Tb, (Ib+mb*Cb**2d0), 0d0, 0d0, 0d0, 0d0,&
	     (L_t*ms1*cos_Tt1+Ct*mt1*cos_Tt1), (L_t*ms1*sin_Tt1+Ct*mt1*sin_Tt1), 0d0, (It+L_t**2d0*ms1+Ct1**2d0*mt1), 0d0, &
	     	Cs1*L_t*ms1*cos(Ts1-Tt1), 0d0,&
	     (L_t*ms2*cos_Tt2+Ct*mt2*cos_Tt2), (L_t*ms2*sin_Tt2+Ct*mt2*sin_Tt2), 0d0, 0d0, (It+L_t**2d0*ms2+Ct2**2d0*mt2), 0d0,&
	     	Cs2*L_t*ms2*cos(Ts2-Tt2),&
	     Cs*ms1*cos_Ts1, Cs*ms1*sin_Ts1, 0d0, Cs*L_t*ms1*cos(Ts1-Tt1), 0d0, (Is1+Cs**2*ms1), 0d0,&
	     Cs*ms2*cos_Ts2, Cs*ms2*sin_Ts2, 0d0, 0d0, Cs*L_t*ms2*cos(Ts2-Tt2), 0d0, (Is2+Cs**2*ms2)/), shape(M)))

	!write(*,*) 'Mass matrix = ', M

	C = transpose(reshape((/0d0, 0d0, mb*Cb*sin_Tb*Tbd, (-ms*L_t*sin_Tt1-mt*Ct*sin_Tt1)*Tt1d, (-ms*L_t*sin_Tt2-mt*Ct*sin_Tt2)*Tt2d, &
				(-ms*Cs*sin_Ts1)*Ts1d, (-ms*Cs*sin_Ts2*Ts2d),&
	     0d0, 0d0, -mb*Cb*cos_Tb*Tbd, (ms*L_t*cos_Tt1+mt*Ct*cos_Tt1)*Tt1d, (ms*L_t*cos_Tt2+mt*Ct*cos_Tt2)*Tt2d, &
	     		(ms*Cs*cos_Ts1)*Ts1d, (ms*Cs*cos_Ts2)*Ts2d,&
	     0d0, 0d0, 0d0, 0d0, 0d0, 0d0, 0d0,&
	     0d0, 0d0, 0d0, 0d0, 0d0, -Cs*L_t*ms1*sin(Ts1-Tt1)*Ts1d, 0d0,&
	     0d0, 0d0, 0d0, 0d0, 0d0, 0d0, -Cs*L_t*ms*sin(Ts2-Tt2)*Ts2d, &
	     0d0, 0d0, 0d0, Cs*L_t*ms*sin(Ts1-Tt1)*Tt1d, 0d0, 0d0, 0d0,&
	     0d0, 0d0, 0d0, 0d0, Cs*L_t*ms*sin(Ts2-Tt2)*Tt2d, 0d0, 0d0/), shape(C)));

	!write(*,*) 'Coriolis matrix = ', C

	!gravity effects
	Gvec = reshape((/0d0, g*(mb+ms1+ms2+mt1+mt2), -g*Cb*mb*sin_Tb, g*(L_t*ms1*sin_Tt1+Ct*mt1*sin_Tt1), &
				g*(L_t*ms2*sin_Tt2+Ct*mt2*sin_Tt2), g*Cs*ms1*sin_Ts1, g*Cs*ms2*sin_Ts2/), shape(Gvec));

	!write(*,*) 'Gravity Effects = ', Gvec
	!Singularity Hard Stops

	if(Tt1-Ts1 < 0) then
	    Tcontrol_clipped(3) = Tcontrol_clipped(3) + 500*(Tt1-Ts1) + 10*(Tt1d-Ts1d);
	endif

	if(Tt2-Ts2 < 0) then
	    Tcontrol_clipped(4) = Tcontrol_clipped(4) + 500*(Tt2-Ts2) +  10*(Tt2d-Ts2d);
	endif
	!Generalized forces due to motor torques
	Tg = reshape((/0d0, 0d0, -(Tcontrol_clipped(1)+Tcontrol_clipped(2)), Tcontrol_clipped(1)-Tcontrol_clipped(3), &
					Tcontrol_clipped(2)-Tcontrol_clipped(4), Tcontrol_clipped(3), Tcontrol_clipped(4)/),&
				 shape(Tg));
	!write(*,*) 'Motor Torques = ', Tg
	!generalized forces due to ground contact
	p1 = (/xh+L_t*sin_Tt1+L_s*sin_Ts1, yh-L_t*cos_Tt1-L_s*cos_Ts1/);
	p2 = (/xh+L_t*sin_Tt2+L_s*sin_Ts2, yh-L_t*cos_Tt2-L_s*cos_Ts2/);

	v1 = (/L_s*cos_Ts1*Ts1d+L_t*cos_Tt1*Tt1d+xhd, L_s*sin_Ts1*Ts1d+L_t*sin_Tt1*Tt1d+yhd/);
	v2 = (/L_s*cos_Ts2*Ts2d+L_t*cos_Tt2*Tt2d+xhd, L_s*sin_Ts2*Ts2d+L_t*sin_Tt2*Tt2d+yhd/);

	N1 = max(-p1(2)*K_g-v1(2)*B_g*max(0d0, -p1(2)), 0d0);
	N2 = max(-p2(2)*K_g-v2(2)*B_g*max(0d0, -p2(2)), 0d0);

	!Model ground contact spring
	if(.NOT.isnan(Tpoint1)) then
	    GS1 = -K_gh*(p1(1)-Tpoint1) - .01*B_g*(v1(1));
	else
	    GS1 = 0d0;
	endif
	if(.NOT.isnan(Tpoint2)) then
	    GS2 = -K_gh*(p2(1)-Tpoint2) - .01*B_g*(v2(1));
	else
	    GS2 = 0d0;
	endif

	!F1 = [(-20*v1(1)/(v1(1)**2+.001)+GS1)*(p1(2) < 0) N1];
	!F2 = [(-20*v2(1)/(v2(1)**2+.001)+GS2)*(p2(2) < 0) N2];
	F1 = (/0d0, N1/);
	F2 = (/0d0, N2/);

	if (p1(2) < 0) then
		F1(1) = GS1;
	endif
	if (p2(2) < 0) then
		F2(1) = GS2;
	endif

	N = reshape((/F1(1)+F2(1), F1(2)+F2(2), 0d0, L_t*cos_Tt1*F1(1)+L_t*sin_Tt1*F1(2), L_t*cos_Tt2*F2(1)+L_t*sin_Tt2*F2(2),&
		 L_s*cos_Ts1*F1(1)+L_s*sin_Ts1*F1(2), L_s*cos_Ts2*F2(1)+L_s*sin_Ts2*F2(2)/), (/7,1/));
	!write(*,*) 'Ground Reaction Forces = ', N

	!gravity + motor torques + ground reaction forces

	!Xdd = M\(Tg+N-C*X(8:14)-G);
	!Xdd = matmul(M, Tg+N-matmul(C,Xmat(8:14,:))-G);

	!write (*,*) 'C    ', C
	!write (*,*) 'Gvec ', Gvec
	!write (*,*) 'N    ', N
	!write (*,*) 'Tg   ', Tg

	Xdd = Tg+N-matmul(C,Xmat(8:14,1:1))-Gvec
	!write (*,*) Xdd
	call DGESV(7, 1, M, 7, IPIV, Xdd, 7, result)
	!write(*,*) 'result=', result
	FiveLinkEOM(1:7) = X(8:14)
	FiveLinkEOM(8:14)= reshape(Xdd, (/7/));

end function FiveLinkEOM

end module BipedStuff
