function [Xt Tt] = Biped(Vx0, simlength, controller, graphics, playback, figtitle)
global g mb mt ms Ib It Is Lt Ls Lb Cb Ct Cs Tcontrol K_g K_gh B_g Tpoint1 Tpoint2
global NNinput NNoutput testnet BipedMovie MovieFrameCount
if nargin < 2
    Vx0 = .5
    simlength = 10;
    graphics = 1;
    controller = 1; %default to the hand tuned controller
    playback = 0;
    figtitle = 'Biped Simulation';
end


%clear all
totAng = 0;
angAvg = 0;
totX = 0;
xAvg = 0;

%start time
st = tic;


%figure and display settings
vx = 2;
vy = 1;

%Dynamic model constants
g = 9.8;
mb = 8;
mt = 1;
ms = 1;
Ib = .1;
It = .01;
Is = .01;
Lb = .6;
Lt = .4;
Ls = .4;
Cb = .3;
Ct = .2;
Cs = .4;
K_g = 8000;
K_gh = 1000;
B_g = 10000;


Tpoint1 = nan;
Tpoint2 = nan;

%State variable x, xd, theta, thetad
X = [-.4 .8 0 1.3 -.1 0 0 Vx0 0 0 0 0 0 0]';
t = [toc(st) toc(st)-.01];
ptime = .002;

%loop counter
i = 0;

V = 0;
mousexp = 0;
Xp = 0;
dt_sum = 0;

NNinput = [];
NNoutput = [];
sample = 1;
tf = 0;

snaptime = .25;
snaps = 1

subplot(snaps,1,1)

if(graphics == 1)
    fig = figure(1);
    axis([-vx vx 0 2*vy]);
    axis equal;
    
    %set up circle sprite
    ang = 0:0.1:2*pi;
    cx = .03*cos(ang);
    cy = .03*sin(ang);
    
end

   RTb = 0;
   RT1 = 0;
   RT2 = 0;
   RS1 = 0;
   RS2 = 0;
   T1 = 0;
   T2 = 0;

%Simulation Loop
for i = 1:100000000

    %Position States
    xh = X(1);
    yh = X(2);
    Tb = X(3);
    Tt1 = X(4);
    Tt2 = X(5);
    Ts1 = X(6);
    Ts2 = X(7);

    %Velocity States
    xhd = X(8);
    yhd = X(9);
    Tbd = X(10);
    Tt1d = X(11);
    Tt2d = X(12);
    Ts1d = X(13);
    Ts2d = X(14);


    
   if (graphics == 1 && mod(i, 5)==0)
       %Refresh the figure
       
       if(snaps ~= 1)
           snapnum = mod(double(int8((tf/snaptime))), snaps)+1
           subplot(snaps, 1, snapnum)
           if(snapnum == snaps)
               pause
           end
           cla
       else
           clf(fig);
       end
       axis([-vx 3*vx 0 2*vy]);
       
       axis equal;
       axis manual;
       title(figtitle);
       %Calculate delta t
       t(2) = t(1);
       t(1) = toc(st);
       
       rh = [xh yh];
       rb = [xh-Lb*sin(Tb) yh+Lb*cos(Tb)];
       rt1 = [xh+Lt*sin(Tt1) yh-Lt*cos(Tt1)];
       rt2 = [xh+Lt*sin(Tt2) yh-Lt*cos(Tt2)];
       rs1 = [xh+Lt*sin(Tt1)+Ls*sin(Ts1) yh-Lt*cos(Tt1)-Ls*cos(Ts1)];
       rs2 = [xh+Lt*sin(Tt2)+Ls*sin(Ts2) yh-Lt*cos(Tt2)-Ls*cos(Ts2)];      
       
       %draw robot
       line([rh(1) rb(1)], [rh(2) rb(2)], 'Color', 'w', 'LineWidth', 4);
       line([rh(1) rt1(1)], [rh(2) rt1(2)], 'Color', 'r', 'LineWidth', 4);
       line([rh(1) rt2(1)], [rh(2) rt2(2)], 'Color', 'b', 'LineWidth', 4);
       line([rt1(1) rs1(1)], [rt1(2) rs1(2)], 'Color', 'b', 'LineWidth', 4);
       line([rt2(1) rs2(1)], [rt2(2) rs2(2)], 'Color', 'r', 'LineWidth', 4);
     
       line(rh(1)+cx, rh(2)+cy, 'LineWidth', 2, 'Color', 'w');
       line(rt1(1)+cx, rt1(2)+cy, 'LineWidth', 2, 'Color', 'w');
       line(rt2(1)+cx, rt2(2)+cy, 'LineWidth',2, 'Color', 'w');
     
       %control targets
       rb = [xh-Lb*sin(RTb) yh+Lb*cos(RTb)];
       rt1 = [xh+Lt*sin(RT1) yh-Lt*cos(RT1)];
       rt2 = [xh+Lt*sin(RT2) yh-Lt*cos(RT2)];
       rs1 = [xh+Lt*sin(RT1)+Ls*sin(RS1) yh-Lt*cos(RT1)-Ls*cos(RS1)];
       rs2 = [xh+Lt*sin(RT2)+Ls*sin(RS2) yh-Lt*cos(RT2)-Ls*cos(RS2)];      
       
       %draw control targets
       %line([rh(1) rb(1)], [rh(2) rb(2)], 'Color', 'k', 'LineWidth', 1);
       %line([rh(1) rt1(1)], [rh(2) rt1(2)], 'Color', 'r', 'LineWidth', 1);
       %line([rh(1) rt2(1)], [rh(2) rt2(2)], 'Color', 'b', 'LineWidth', 1);
       %line([rt1(1) rs1(1)], [rt1(2) rs1(2)], 'Color', 'b', 'LineWidth', 1);
       %line([rt2(1) rs2(1)], [rt2(2) rs2(2)], 'Color', 'r', 'LineWidth', 1);
       
       
       %make sure the graphics have time to update
       pause(ptime);
       %getframe(fig)
       if(~isempty(MovieFrameCount))
           BipedMovie(:,MovieFrameCount)=getframe(fig); 
           MovieFrameCount = MovieFrameCount + 1;
       end
   end
   %dt_sum = dt_sum + dt1;
   dt1 = .01;
   
   %Simulate
   %TOUT = 0;
   %YOUT = 0;
   
   x = X(1);
   xd = X(2);

   
   %Tcontrol = [0 0 0 0];
   
   %simple walking controller
   
   p1 = [xh+Lt*sin(Tt1)+Ls*sin(Ts1) yh-Lt*cos(Tt1)-Ls*cos(Ts1)];
   p2 = [xh+Lt*sin(Tt2)+Ls*sin(Ts2) yh-Lt*cos(Tt2)-Ls*cos(Ts2)];
   
   gzone = .05;
   
   %Touchdown point tracking
   if(p1(2) < gzone && isnan(Tpoint1))
       Tpoint1 = p1(1);
   elseif (p1(2) > gzone && ~isnan(Tpoint1))
       Tpoint1 = nan;
   end
   
   if(p2(2) < gzone && isnan(Tpoint2))
       Tpoint2 = p2(1);
   elseif (p2(2) > gzone && ~isnan(Tpoint2))
       Tpoint2 = nan;
   end
   %disp( ['Tpoint1 = ', num2str(Tpoint1), 'Tpoint2 = ', num2str(Tpoint2)]);
   %pause
   
   %disp(horzcat('Sim Time = ', num2str(tf), '/', num2str(simlength)))
   
   if(Tt1 > Tt2)
       p_lead = p1;
       p_lag = p2;
       Tt_lead = Tt1;
       Tt_lag = Tt2;
       Ts_lead = Ts1;
       Ts_lag = Ts2;
   else
       p_lead = p2;
       p_lag = p1;
       Tt_lead = Tt2;
       Tt_lag = Tt1;
       Ts_lead = Ts2;
       Ts_lag = Ts1;
   end
   
   %Determine contact state
   if(p_lead(2) < gzone)
       C1 = 1;
   else
       C1 = 0;
   end
   if(p_lag(2) < gzone)
       C2 = 1;
   else
       C2 = 0;
   end
   
   input_sample = [yh, xhd, yhd, Tb, Tt1, Tt2, Ts1, Ts2, C1, C2];
    
   if(controller == 1)
       if(p_lead(2) < gzone)
           if(p_lag(2) < gzone) %double support
               RTb = -.2;
               RT_lead = 0;
               RT_lag = 1;
               RS_lead = 0;
               RS_lag = -.9;
           else %Single support on leading leg
               RTb = -.1;
               RT_lead = 0;
               RT_lag = .9;
               RS_lead = 0;
               if(Tt_lag<.6) RS_lag = -1.5;
               else RS_lag = 1;
               end
           end

       else
           if(p_lag(2) < gzone) %Single support on trailing leg
               RTb = -.1;
               RT_lead = .9;
               RT_lag = 0;
               if(Tt_lead<.6) RS_lead = -1.5;
               else RS_lead = 1;
               end
               RS_lag = 0;
           else %Flying
               RTb = -.1;
               RT_lead = 1.2;
               RT_lag = -.4;
               RS_lead = .2;
               RS_lag = -.4;
           end
       end
   elseif(controller == 2)
        %out1 = sim(testnet, input_sample');
        out = mySigmoidalNetwork(testnet, input_sample');
        %out1-out
        %out = out1;
        
        RT_lead = out(1);
        RT_lag = out(2);
        RS_lead = out(3);
        RS_lag = out(4);
        %RTb = out(5);
   else
       RT_lead = 0;
       RT_lag = 0;
       RS_lead = 0;
       RS_lag = 0;
   end
   
   output_sample = [RT_lead RT_lag RS_lead RS_lag];
   NNinput = [NNinput;input_sample];
   NNoutput = [NNoutput;output_sample];
   
   if(Tt1 > Tt2)
       RT1 = RT_lead;
       RT2 = RT_lag;
       RS1 = RS_lead;
       RS2 = RS_lag;
   else
       RT2 = RT_lead;
       RT1 = RT_lag;
       RS2 = RS_lead;
       RS1 = RS_lag;
   end

   
   
   Kbod = 1000;
   Kleg = 600;
   Kleg = 400;
   Kshank = 300;
   Kshank = 400;
   Bleg = 30;
   
   Tcontrol = [Kleg*(RT1-Tt1)-Kbod*(RTb-Tb)-Bleg*(Tt1d) Kleg*(RT2-Tt2)-Kbod*(RTb-Tb)-Bleg*(Tt2d) Kshank*(RS1-Ts1)-Bleg*(Ts1d-Tt1d) Kshank*(RS2-Ts2)-Bleg*(Ts2d-Tt2d)]';
   
   
   
   
   %NNinput(sample, :) = X';
   %NNoutput(sample, :) = F;
   sample = sample + 1;
 
   %some ode method
   %options = odeset('RelTol',1e-3,'AbsTol',1e-3*ones(1,14));
   %[TOUT, YOUT] = ode45(@FiveLinkEOM, [0 dt1], X, options);
   %X = YOUT(size(YOUT, 1), :);
   
   %Alternative methods
   nt = 10;
   dt = dt1/nt;
   
   for j = 1:nt
       %NNinput = [NNinput ; FiveLinkEOM(0, X)'];
       %NNoutput = [NNoutput ; F];
       %forward euler
       %X = X + FiveLinkEOM(0, X)*dt;

       %midpoint method
       %X = X+dt*FiveLinkEOM(0, X+0.5*dt*FiveLinkEOM(0, X));

       %Heuns Method
       X_bar = X + dt*FiveLinkEOM(0, X);
       X = X + .5*dt*(FiveLinkEOM(0, X)+FiveLinkEOM(0, X_bar));
   end
   
   tf = tf+dt1;
   %tf = toc(st);
   time(i) = tf;
   
   Xt(:,i) = X;
   
   totAng = totAng + abs(sin(X(3)));
   totX = totX + abs(X(1));
   
   %end condition - time runs out or pendulum falls below cart
   if(tf > simlength || yh < 0 || abs(Tb) > pi/2)
       %Return average angle from entirety of test
       break;
   end
   
end

Tt = time;

if(playback)
    st = tic
    %Real time playback
    for i = 1:100000000

        t = toc(st)


        while (t > time(i))
           i = i+1;
           if(i >= size(Xt, 2))
                break;
           end
        end
        X = Xt(:, i);
        tf = time(i);

        %Position States
        xh = X(1);
        yh = X(2);
        Tb = X(3);
        Tt1 = X(4);
        Tt2 = X(5);
        Ts1 = X(6);
        Ts2 = X(7);

       if (graphics == 1)
           %Refresh the figure
           clf(fig);
           clf(fig);
           axis([-vx 3*vx 0 2*vy]);

           axis equal;
           axis manual;
           rh = [xh yh];
           rb = [xh-Lb*sin(Tb) yh+Lb*cos(Tb)];
           rt1 = [xh+Lt*sin(Tt1) yh-Lt*cos(Tt1)];
           rt2 = [xh+Lt*sin(Tt2) yh-Lt*cos(Tt2)];
           rs1 = [xh+Lt*sin(Tt1)+Ls*sin(Ts1) yh-Lt*cos(Tt1)-Ls*cos(Ts1)];
           rs2 = [xh+Lt*sin(Tt2)+Ls*sin(Ts2) yh-Lt*cos(Tt2)-Ls*cos(Ts2)];      

           %draw robot
           line([rh(1) rb(1)], [rh(2) rb(2)], 'Color', 'k', 'LineWidth', 4);
           line([rh(1) rt1(1)], [rh(2) rt1(2)], 'Color', 'r', 'LineWidth', 4);
           line([rh(1) rt2(1)], [rh(2) rt2(2)], 'Color', 'b', 'LineWidth', 4);
           line([rt1(1) rs1(1)], [rt1(2) rs1(2)], 'Color', 'b', 'LineWidth', 4);
           line([rt2(1) rs2(1)], [rt2(2) rs2(2)], 'Color', 'r', 'LineWidth', 4);

           line(rh(1)+cx, rh(2)+cy, 'LineWidth', 4);
           line(rt1(1)+cx, rt1(2)+cy, 'LineWidth', 4);
           line(rt2(1)+cx, rt2(2)+cy, 'LineWidth', 4);

           %make sure the graphics have time to update
           pause(ptime);
       end
       %dt_sum = dt_sum + dt1;
       if(i >= size(Xt, 2))

           break;
       end


    end
end
    %dtave = dt_sum/(i-1);
    %average_frequency = 1/dtave


%figure(2)
%plot(time, State)

end