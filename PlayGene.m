function PlayGene( gen, ind, simtime)

global testnet

clc
Nweights = 64
%get the neural net ready to simulate
gene = ones(1, Nweights);

%testnet = feedforwardnet(5);
%testnet = configure(testnet, [-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi;-pi pi]);

testnet = feedforwardnet(4);
testnet = configure(testnet, [-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi]);

testnet = init(testnet);

testnet = setwb(testnet, gene);

disp('Loading Genepool')
filename = strcat('GeneScores', num2str(gen), '.mat')
load(filename);

Vx0 = .5;
%load up the current individual
testnet = setwb(testnet, genes(ind, :)');
[Xt Tt] = Biped(Vx0, simtime, 2, 1, 0);
