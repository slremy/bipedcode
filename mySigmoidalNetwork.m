function out = mySigmoidalNetwork(net, input)

in = input;

%Scale input
%in = (min(max(in, -5), 5));
in = in./5;



Nin = length(in);
IW = net.IW{1,1};
B1 = net.b{1};
%b1 = B1(1);
LW = net.LW{2,1};
b2 = net.b{2};
input_result = IW*in+B1;
%input_result = (min(max(input_result, -5), 5));
h = tansig(input_result);
out = LW*h + b2;

%Scale output
%out = out*pi-pi/2;
out = out.*pi;