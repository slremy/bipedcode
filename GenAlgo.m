%Outline for GA:
%   Generate random pop
%   while loop(
%   evaluate fitness of each chromosome
%   create new pop:
%       select 2 parents probabalistically based on fitness
%       cross over parents to get new offspring w/ crossover probability
%       mutate new offspring at each locus with mutation probability
%       place new offspring in pop
%   use new generation in algo
%   if end condition met, stop and return best solution)

global testnet

clc
Nweights = 64
%get the neural net ready to simulate
gene = ones(1, Nweights);

%testnet = feedforwardnet(5);
%testnet = configure(testnet, [-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi;-pi pi]);

testnet = feedforwardnet(4);
testnet = configure(testnet, [-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi]);

testnet = init(testnet);

testnet = setwb(testnet, gene);

pop = 10;
runs = 5;
simtime = 5;
totProb = 0;
mutation_rate = .05;
startfresh = 1;

if(startfresh==1)
    disp(['Starting Fresh'])
    genes = (rand(pop, Nweights)-.5);
    
    load('goodwb.mat')
    
    genes(1, :) = goodWB';
    
    score = GetScores(genes);
    save('GeneScores1.mat', 'genes', 'score');
    generations = 1;
else
    generations = 1;
    disp('Loading Genepool')
    filename = strcat('GeneScores', num2str(generations), '.mat')
    load(filename);
end

while(generations < 100)
    [y, I] = max(score);
    best = genes(I,:);
    average_score = mean(score)
    disp(['Generation ',num2str(generations), ' Average Score= ', num2str(average_score)])
    %get rid of any negative stuff
    score_floored = max(score, 0) 
    totScore = sum(score_floored);
    prob = cumsum(score_floored);
    index_array = (1:pop)';

    selected = [];
    numleft = pop;
    for i = 1:2*(pop-2)
        randy = rand()*totScore;

        testNo = 1;

        %Choose a test
        while prob(testNo) < randy
            testNo = testNo + 1;
        end
        testNo;
        selected(i) = index_array(testNo);

        %Remove test from array
    %     totScore = totScore-score(index_array(testNo));
    %     if testNo == 1
    %         prob = prob(2:numleft)-score(index_array(testNo));
    %         index_array = index_array(2:numleft);
    %     else if testNo == numleft
    %         prob = prob(1:testNo-1);
    %         index_array = index_array(1:testNo-1);
    %         else
    %         prob = [prob(1:testNo-1) ; prob(testNo+1:numleft)-score(index_array(testNo))];
    %         index_array = [index_array(1:testNo-1) ; index_array(testNo+1:numleft)];
    %         end
    %     end
    %     numleft = numleft - 1;

    end

    %index list for mating genes
    selected

    %Have chosen breeding population, perform mating to get next generationg
    for i = 1:pop-2
        
        geneA = genes(selected((i-1)*2+1), :);
        geneB = genes(selected((i-1)*2+2), :);
        n = length(geneA);

        %crossover
        cross_point = int32((n-1)*rand(1))+1;
        geneA1 = [geneA(1:cross_point) geneB(cross_point+1:n)];
        geneA2 = [geneB(1:cross_point) geneA(cross_point+1:n)];

        scoreA1 = GetScores(geneA1);
        scoreA2 = GetScores(geneA2);
        
        if(scoreA1 > scoreA2)
            geneNew = geneA1;
        else
            geneNew = geneA2;
        end

        for(j = 1:n)
            r = rand(1);
            if(r > mutation_rate)
                geneNew(j) = geneNew(j)*(1+.1*(rand(1)-.5));
            end
        end
        genes(i, :) = geneNew;
    end
    %Add one random individual each time
    genes(pop-1, :) = rand(1, Nweights);
    genes(pop, :) = best;
    
    %Simulate next generation
    generations = generations+1;
    score = GetScores(genes);
    
    filename = strcat('GeneScores', num2str(generations), '.mat');
    save(filename, 'genes', 'score');
end

