
global testnet BipedMovie MovieFrameCount

clc
BipedMovie = getframe(figure(1));
MovieFrameCount = 1;
Nweights = 64
%get the neural net ready to simulate
gene = ones(1, Nweights);

%testnet = feedforwardnet(5);
%testnet = configure(testnet, [-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi;-pi pi]);

testnet = feedforwardnet(4);
testnet = configure(testnet, [-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi]);

testnet = init(testnet);

testnet = setwb(testnet, gene);

pop = 10;
runs = 5;
simtime = 5;
totProb = 0;
mutation_rate = .05;
startfresh = 1;
generations = 75;

while(generations < 76)
    disp('Loading Genepool')
    filename = strcat('GeneScores', num2str(generations), '.mat')
    load(filename);
    %play just best walkers
    %[s, i] = max(score);
    %GetScores(genes(i,:), ['Best of Generation ' num2str(generations)]);
    
    %or play a whole generation
    GetScores(genes, ['Generation ' num2str(generations) ', Individual ']);
    
    generations = generations+5;
end
movie2avi(BipedMovie,'movie100.avi', 'compression', 'none');
