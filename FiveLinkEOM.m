function [XD] = FiveLinkEOM(t, X )
global g mb mt ms Ib It Is Lt Ls Cb Ct Cs Tcontrol K_g K_gh B_g Tpoint1 Tpoint2

%XD = 0*X;
%return

Tmax = 50;
%Saturation
Tcontrol = max(min(Tcontrol, ones(4,1)*Tmax), -ones(4,1)*Tmax); 

%Position States
xh = X(1);
yh = X(2);
Tb = X(3);
Tt1 = X(4);
Tt2 = X(5);
Ts1 = X(6);
Ts2 = X(7);

%Velocity States
xhd = X(8);
yhd = X(9);
Tbd = X(10);
Tt1d = X(11);
Tt2d = X(12);
Ts1d = X(13);
Ts2d = X(14);

cos_Tb = cos(Tb);
sin_Tb = sin(Tb);
cos_Tt1 = cos(Tt1);
sin_Tt1 = sin(Tt1);
cos_Tt2 = cos(Tt2);
sin_Tt2 = sin(Tt2);
cos_Ts1 = cos(Ts1);
sin_Ts1 = sin(Ts1);
cos_Ts2 = cos(Ts2);
sin_Ts2 = sin(Ts2);




mt1 = mt;
mt2 = mt;
ms1 = ms;
ms2 = ms;
Ct1 = Ct;
Ct2 = Ct;
Cs1 = Cs;
Cs2 = Cs;
Is1 = Is;
Is2 = Is;


M = [mb+mt1+mt2+ms1+ms2 0 -mb*Cb*cos_Tb mt1*Ct*cos_Tt1+ms1*Lt*cos_Tt1 mt2*Ct*cos_Tt2+ms2*Lt*cos_Tt2 ms1*Cs*cos_Ts1 ms2*Cs*cos_Ts2;
     0 mb+mt1+mt2+ms1+ms2 -mb*Cb*sin_Tb mt1*Ct*sin_Tt1+ms1*Lt*sin_Tt1 mt2*Ct*sin_Tt2+ms2*Lt*sin_Tt2 ms1*Cs*sin_Ts1 ms2*Cs*sin_Ts2;
     -Cb*mb*cos_Tb -Cb*mb*sin_Tb (Ib+Cb^2*mb) 0 0 0 0;
     (Lt*ms1*cos_Tt1+Ct*mt1*cos_Tt1) (Lt*ms1*sin_Tt1+Ct*mt1*sin_Tt1) 0 (It+Lt^2*ms1+Ct1^2*mt1) 0 Cs1*Lt*ms1*cos(Ts1-Tt1) 0;
     (Lt*ms2*cos_Tt2+Ct*mt2*cos_Tt2) (Lt*ms2*sin_Tt2+Ct*mt2*sin_Tt2) 0 0 (It+Lt^2*ms2+Ct2^2*mt2) 0 Cs2*Lt*ms2*cos(Ts2-Tt2);
     Cs*ms1*cos_Ts1 Cs*ms1*sin_Ts1 0 Cs*Lt*ms1*cos(Ts1-Tt1) 0 (Is1+Cs^2*ms1) 0;
     Cs*ms2*cos_Ts2 Cs*ms2*sin_Ts2 0 0 Cs*Lt*ms2*cos(Ts2-Tt2) 0 (Is2+Cs^2*ms2)];
     
C = [0 0 mb*Cb*sin_Tb*Tbd (-ms*Lt*sin_Tt1-mt*Ct*sin_Tt1)*Tt1d (-ms*Lt*sin_Tt2-mt*Ct*sin_Tt2)*Tt2d (-ms*Cs*sin_Ts1)*Ts1d (-ms*Cs*sin_Ts2*Ts2d);
     0 0 -mb*Cb*cos_Tb*Tbd (ms*Lt*cos_Tt1+mt*Ct*cos_Tt1)*Tt1d (ms*Lt*cos_Tt2+mt*Ct*cos_Tt2)*Tt2d (ms*Cs*cos_Ts1)*Ts1d (ms*Cs*cos_Ts2)*Ts2d;
     0 0 0 0 0 0 0;
     0 0 0 0 0 -Cs*Lt*ms1*sin(Ts1-Tt1)*Ts1d 0;
     0 0 0 0 0 0 -Cs*Lt*ms*sin(Ts2-Tt2)*Ts2d;
     0 0 0 Cs*Lt*ms*sin(Ts1-Tt1)*Tt1d 0 0 0;
     0 0 0 0 Cs*Lt*ms*sin(Ts2-Tt2)*Tt2d 0 0];

%gravity effects
G = [0 g*(mb+ms1+ms2+mt1+mt2) -g*Cb*mb*sin_Tb g*(Lt*ms1*sin_Tt1+Ct*mt1*sin_Tt1) g*(Lt*ms2*sin_Tt2+Ct*mt2*sin_Tt2) g*Cs*ms1*sin_Ts1 g*Cs*ms2*sin_Ts2]';

%Singularity Hard Stops
if(Tt1-Ts1 < 0)
    Tcontrol(3) = Tcontrol(3) + 500*(Tt1-Ts1) + 10*(Tt1d-Ts1d);
end

if(Tt2-Ts2 < 0)
    Tcontrol(4) = Tcontrol(4) + 500*(Tt2-Ts2) +  10*(Tt2d-Ts2d);
end

%Generalized forces due to motor torques
T = [0 0 -(Tcontrol(1)+Tcontrol(2)) Tcontrol(1)-Tcontrol(3) Tcontrol(2)-Tcontrol(4) Tcontrol(3) Tcontrol(4)]';

%generalized forces due to ground contact
p1 = [xh+Lt*sin_Tt1+Ls*sin_Ts1 yh-Lt*cos_Tt1-Ls*cos_Ts1];
p2 = [xh+Lt*sin_Tt2+Ls*sin_Ts2 yh-Lt*cos_Tt2-Ls*cos_Ts2];

v1 = [Ls*cos_Ts1*Ts1d+Lt*cos_Tt1*Tt1d+xhd Ls*sin_Ts1*Ts1d+Lt*sin_Tt1*Tt1d+yhd];
v2 = [Ls*cos_Ts2*Ts2d+Lt*cos_Tt2*Tt2d+xhd Ls*sin_Ts2*Ts2d+Lt*sin_Tt2*Tt2d+yhd];

N1 = max(-p1(2)*K_g-v1(2)*B_g*max(0, -p1(2)), 0);
N2 = max(-p2(2)*K_g-v2(2)*B_g*max(0, -p2(2)), 0);

%Model ground contact spring
if(~isnan(Tpoint1))
    GS1 = -K_gh*(p1(1)-Tpoint1) - .01*B_g*(v1(1));
else
    GS1 = 0;
end
if(~isnan(Tpoint2))
    GS2 = -K_gh*(p2(1)-Tpoint2) - .01*B_g*(v2(1));
else
    GS2 = 0;
end

%F1 = [(-20*v1(1)/(v1(1)^2+.001)+GS1)*(p1(2) < 0) N1];
%F2 = [(-20*v2(1)/(v2(1)^2+.001)+GS2)*(p2(2) < 0) N2];
F1 = [(GS1)*(p1(2) < 0) N1];
F2 = [(GS2)*(p2(2) < 0) N2];

N = [F1(1)+F2(1) F1(2)+F2(2) 0 Lt*cos_Tt1*F1(1)+Lt*sin_Tt1*F1(2) Lt*cos_Tt2*F2(1)+Lt*sin_Tt2*F2(2) Ls*cos_Ts1*F1(1)+Ls*sin_Ts1*F1(2) Ls*cos_Ts2*F2(1)+Ls*sin_Ts2*F2(2)]';
%gravity + motor torques + ground reaction forces

Xdd = M\(T+N-C*X(8:14)-G);
XD = [X(8:14); Xdd];

end

