global NNinput NNoutput testnet

%1 = hand tuned, 2 = NN
controller = 1;

NNinput = []
NNoutput = []
testnet = 0

loaddata = 0;
if(loaddata == 0)

    totalNNinput = [];
    totalNNoutput = [];

    runs = 10
    %human data gathering
    length = 5;
    for i = 1:runs
        i
        vx0 = .5+.1*((i/runs)-.5);
        
        Biped(vx0, length, controller, 1, 0);
        totalNNinput = [totalNNinput ; NNinput];
        totalNNoutput = [totalNNoutput ; NNoutput];
        pause(.1);
    end
    if controller == 1
        save('basedata.mat', 'totalNNinput', 'totalNNoutput')
    else
        save('otherdata.mat', 'totalNNinput', 'totalNNoutput')
    end
else
    totalNNinput = [0 0 0 0];
    totalNNoutput = 0;
    if controller == 1
        load('basedata.mat')
    else
        load('otherdata.mat')
    end
end


testnet = newff([-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5;-5 5], [-pi pi;-pi pi;-pi pi;-pi pi;-pi pi], 4);
testnet.trainParam.epochs = 50;
testnet = train(testnet,totalNNinput',totalNNoutput');
 
%[ang(i), time(i)] = Pendulum(1, .001, length, 3);

NNperf = sim(testnet, totalNNinput')

plot(NNperf(1, :), 'b', 'linewidth', 3)
hold on
plot(totalNNoutput(:,1)', 'Color', 'r')

